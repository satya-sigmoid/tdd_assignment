class Calculator:
    def addition(self, a, b):
        return a + b

    def multiplication(self, a, b):
        return a + b

    def subtraction(self, a, b):
        return a - b

    def division(self, a, b):
        assert b!=0
        return a / b

    def lcm(self,a, b):
        L = a if a > b else b
        while L <= a * b:
            if L % a == 0 and L % b == 0:
                return L
            L += 1

    # calculating HCF
    def hcf(self,a, b):
        H = a if a < b else b
        while H >= 1:
            if a % H == 0 and b % H == 0:
                return H
            H -= 1
            
    def read_from_file(self, filename):
        infile = open(filename, "r")
        line = infile.readline()
        return line        