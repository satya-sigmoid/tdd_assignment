import unittest, pytest
from Calculator import Calculator


@pytest.fixture()
def object():
    calculate = Calculator()
    return calculate


def setup_module(module):
    print("\nSetting up Module")


def teardown_module(module):
    print("\nTearing Down module")


class TestCalculator:
    @classmethod
    def setup_class(cls):
        print("\nSetUp Class")

    def teardown_class(cls):
        print("\nTearing Down Class")

    def setup_method(self,function):
        print(f"\nSetting up method")

    def teardown_method(self,function):
        print(f"\nTearing down method")

    def test_add(self, object):
        assert object.addition(1, 1) == 2

    def test_mult(self, object):
        assert object.multiplication(2, 2) == 4

    def test_sub(self, object):
        assert object.subtraction(2, 6) == -4

    def test_div(self, object):
        assert object.division(60, 12) == 5

    def test_lcm(self, object):
        assert object.lcm(60, 12) == 60

    def test_lcm(self, object):
        assert object.hcf(60, 12) == 12
        
    def test_return(self, object, monkeypatch):
        mock_file = MagicMock()
        mock_file.readline = MagicMock(return_value="realme narzo 20 pro")
        mock_open = MagicMock(return_value=mock_file)
        monkeypatch.setattr("builtins.open", mock_open)
        result = object.read_from_file("mobile")
        mock_open.assert_called_once_with("mobile", "r")
        assert result == "realme narzo 20 pro"     


